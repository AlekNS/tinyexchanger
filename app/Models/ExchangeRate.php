<?php

namespace App\Models;

use App\Models\Helpers\CompositePrimaryKey;
use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model
{
    use CompositePrimaryKey;

    protected $table = 'exchange_rates';

    protected $fillable = [
        'base', 'related', 'rate',
    ];

    protected $primaryKey = ['base', 'related'];
    public $incrementing = false;

    public $timestamps = false;
}
