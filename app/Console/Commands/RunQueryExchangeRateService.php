<?php

namespace App\Console\Commands;

use App\Commands\UpdateExchangeRatesCommand;
use App\Services\ExchangeRate\Currency;
use App\Services\ExchangeRate\QueryRates;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\Console\Output\Output;

class RunQueryExchangeRateService extends Command
{
    protected $signature = 'service:runexrate {sleep=10}';

    protected $description = 'Update exchange rates';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info('Start updating of exchange rates...');
        while(true) {
            $this->info('Query... ' . new Carbon());
            dispatch(new UpdateExchangeRatesCommand(new QueryRates([
                [Currency::RUB, Currency::USD],
                [Currency::RUB, Currency::EUR]
            ])));
            sleep(min(max($this->argument('sleep'), 5), 120));
        }
    }
}
