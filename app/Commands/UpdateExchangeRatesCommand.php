<?php

namespace App\Commands;

use App\Services\ExchangeRate\QueryRates;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateExchangeRatesCommand extends Command implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $queryRates;

    public function __construct(QueryRates $queryRates)
    {
        $this->queryRates = $queryRates;
    }

    /**
     * @return QueryRates
     */
    public function getQueryRates()
    {
        return $this->queryRates;
    }
}