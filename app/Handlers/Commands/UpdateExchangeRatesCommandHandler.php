<?php

namespace App\Handlers\Commands;

use App\Commands\UpdateExchangeRatesCommand;
use App\Models\ExchangeRate;
use App\Services\ExchangeRate\Currency;
use App\Services\ExchangeRate\QueryRates;
use App\Services\ExchangeRate\RateService;
use App\Services\ExchangeRate\Providers\GroupProvider;
use App\Services\ExchangeRate\Providers\CbrProvider;
use App\Services\ExchangeRate\Providers\YahooProvider;

use DB;

class UpdateExchangeRatesCommandHandler
{
    public function handle(UpdateExchangeRatesCommand $command)
    {
        // @TODO: Should be read from config
        $service = new RateService(
            (new GroupProvider())->add([
                new CbrProvider(),
                new YahooProvider()
            ])
        );

        // @TODO: Should be read from config
        $allRates = $service->getRatesFor($command->getQueryRates());

        DB::transaction(function () use ($allRates) {
            foreach($allRates as $base => $relatedRates) {
                /** @var Currency $rate */
                foreach($relatedRates as $rate) {
                    ExchangeRate::updateOrCreate(
                        ['base' => $base, 'related' => $rate->getName()],
                        [
                            'rate' => $rate->getValue(),
                            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
                        ]
                    );
                }
            }
        });
    }
}
