<?php

namespace App\Services\ExchangeRate\Providers;

use App\Services\ExchangeRate\Currency;
use App\Services\ExchangeRate\IProvider;
use App\Services\ExchangeRate\QueryRates;

abstract class AbstractProvider implements IProvider
{
    public function query(QueryRates $queryRates)
    {
        return $this->parse($this->requestData($queryRates), $queryRates);
    }

    abstract protected function parse($stringContent, QueryRates $queryRates);
    abstract protected function requestData(QueryRates $queryRates);
}
