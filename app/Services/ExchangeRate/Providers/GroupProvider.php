<?php

namespace App\Services\ExchangeRate\Providers;

use App\Services\ExchangeRate\Currency;
use App\Services\ExchangeRate\IProvider;
use App\Services\ExchangeRate\QueryRates;

class GroupProvider implements IProvider
{
    /**
     * @var IProvider[]
     */
    protected $providers = [];

    public function add(array $providers)
    {
        $this->providers = array_merge($this->providers, $providers);
        return $this;
    }

    public function query(QueryRates $queryRates)
    {
        foreach($this->providers as $provider) {
            try {
                return $provider->query($queryRates);
            } catch (\Exception $ex) {
                // @TODO: Add checking of only HTTP Exceptions
            }
        }
        throw new \RuntimeException('All queries of exchange rate providers are failed.');
    }
}
