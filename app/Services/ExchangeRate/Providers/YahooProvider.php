<?php

namespace App\Services\ExchangeRate\Providers;

use App\Services\ExchangeRate\Currency;
use App\Services\ExchangeRate\QueryRates;
use Guzzle\Http\Client;

class YahooProvider extends AbstractProvider
{
    protected function parse($stringContent, QueryRates $queryRates)
    {
        $rates = [];
        $parsedRates = json_decode($stringContent, true);
        if (isset($parsedRates['query']['results']['rate'])) {
            foreach($parsedRates['query']['results']['rate'] as $rate) {
                $currencies = explode('/', $rate['Name']);
                if (!$queryRates->isPairExists($currencies[1], $currencies[0])) {
                    continue;
                }
                $rates[$currencies[1]][$currencies[0]] = (double)str_replace(',', '.', $rate['Rate']);
            }
        }
        return $rates;
    }

    protected function requestData(QueryRates $queryRates)
    {
        $client = new Client('https://query.yahooapis.com');
        $response = $client->get('/v1/public/yql?q=select+*+from+yahoo.finance.xchange+where+pair+=+%22USDRUB,EURRUB%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=')->send();
        return $response->getBody(true);
    }
}
