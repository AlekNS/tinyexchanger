<?php

namespace App\Services\ExchangeRate\Providers;

use App\Services\ExchangeRate\Currency;
use App\Services\ExchangeRate\IProvider;
use App\Services\ExchangeRate\QueryRates;
use Guzzle\Http\Client;

class CbrProvider extends AbstractProvider
{
    public function query(QueryRates $queryRates)
    {
        foreach($queryRates->getPairs() as $pair) {
            if ($pair[0] != Currency::RUB) {
                throw new \RuntimeException('Supported only RUB as base currency');
            }
        }

        return parent::query($queryRates);
    }

    protected function readValCurs(\XMLReader $reader, array &$rates, $queryRates)
    {
        while($reader->read() && $reader->name == 'ValCurs');
        while($this->readValute($reader, $rates, $queryRates));
    }

    protected function readValute(\XMLReader $reader, array &$rates, $queryRates)
    {
        $charCode = '';
        $value = '';
        while ($reader->read()) {
            if ($reader->nodeType == \XMLReader::END_ELEMENT) {
                if ($reader->name == 'Valute') {
                    if ($charCode) {
                        $rates[$charCode] = (double)str_replace(',', '.', $value);
                    }
                    return true;
                }
            }
            if ($reader->nodeType == \XMLReader::ELEMENT) {
                switch($reader->name) {
                    case 'CharCode':
                        $charCode = $reader->readString();
                        if (!$queryRates->isPairExists(Currency::RUB, $charCode)) {
                            $charCode = '';
                            continue;
                        }
                        break;
                    case 'Value':
                        $value = $reader->readString();
                        break;
                }
            }
        }
        return false;
    }

    protected function parse($stringContent, QueryRates $queryRates)
    {
        $rates = [Currency::RUB => []];

        $xmlReader = new \XMLReader();
        $xmlReader->XML($stringContent);
        $this->readValCurs($xmlReader, $rates[Currency::RUB], $queryRates);

        return $rates;
    }

    protected function requestData(QueryRates $queryRates)
    {
        $client = new Client('http://www.cbr.ru');
        $response = $client->get('/scripts/XML_daily.asp')->send();
        return $response->getBody(true);
    }
}
