<?php

namespace App\Services\ExchangeRate;

final class QueryRates
{
    private $pairsHash;
    private $pairs;

    public function __construct(array $pairs)
    {
        foreach($pairs as $pair) {
            if (count($pair) != 2) {
                throw new \InvalidArgumentException('Element isnt pair');
            }
            if (!Currency::exists($pair[0]) || !Currency::exists($pair[1])) {
                throw new \InvalidArgumentException('Unknown currency');
            }
            if ($pair[0] === $pair[1]) {
                throw new \InvalidArgumentException('Equal pair');
            }
        }

        $this->pairs = $pairs;
        $this->pairsHash = [];
        foreach($pairs as $pair) {
            $this->pairsHash[$pair[0]][$pair[1]] = true;
        }
    }

    public function isPairExists($currency1, $currency2)
    {
        return isset($this->pairsHash[$currency1][$currency2]) && $this->pairsHash[$currency1][$currency2];
    }

    /**
     * @return string[]
     */
    public function getPairs()
    {
        return $this->pairs;
    }
}
