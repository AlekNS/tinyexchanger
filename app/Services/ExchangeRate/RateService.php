<?php

namespace App\Services\ExchangeRate;

class RateService
{
    protected $provider;

    public function __construct(IProvider $provider)
    {
        $this->provider = $provider;
    }

    public function getRatesFor(QueryRates $queryRates)
    {
        $rates = [];

        foreach($this->provider->query($queryRates) as $baseCurrency => $relatedCurrencies) {
            foreach($relatedCurrencies as $currencyName => $value) {
                $rates[$baseCurrency][] = new Currency($currencyName, $value);
            }
        }

        return $rates;
    }
}