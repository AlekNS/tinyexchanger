<?php

namespace App\Services\ExchangeRate;

final class Currency
{
    const USD = 'USD';
    const RUB = 'RUB';
    const EUR = 'EUR';

    private $name;
    private $value;

    public function __construct($name, $value)
    {
        if (!self::exists($name)) {
            throw new \InvalidArgumentException('Currency isnt available');
        }

        if (!is_numeric($value) || $value < 0) {
            throw new \InvalidArgumentException('Value should greater or equals 0');
        }

        $this->name = $name;
        $this->value = (double)$value;
    }

    public static function all()
    {
        return [self::USD, self::RUB, self::EUR];
    }

    public static function exists($val)
    {
        return in_array($val, self::all());
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return double
     */
    public function getValue()
    {
        return $this->value;
    }
}