<?php

namespace App\Services\ExchangeRate;

interface IProvider
{
    public function query(QueryRates $queryRates);
}