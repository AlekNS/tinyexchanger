<?php

use \App\Services\ExchangeRate\Currency;
use \App\Services\ExchangeRate\Providers\CbrProvider;
use \App\Services\ExchangeRate\Providers\YahooProvider;
use \App\Services\ExchangeRate\QueryRates;

class CbrMockedProvider extends CbrProvider
{
    protected function requestData(QueryRates $queryRates)
    {
        return file_get_contents(__DIR__.'/mocks/cbr.xml');
    }
}

class YahooMockedProvider extends YahooProvider
{
    protected function requestData(QueryRates $queryRates)
    {
        return file_get_contents(__DIR__.'/mocks/yahoo.json');
    }
}

class ExchangeRateTest extends TestCase
{
    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldCurrencyExceptionOnWrongName()
    {
        new Currency('', 1);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldCurrencyExceptionOnWrongValue()
    {
        new Currency(Currency::RUB, -1);
    }

    public function testShouldCurrencyMatch()
    {
        $cur = new Currency(Currency::RUB, 10.512389);
        $this->assertEquals($cur->getName(), Currency::RUB);
        $this->assertEquals($cur->getValue(), 10.512389);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldQueryRatesThrowExceptionOnInvalidArguments()
    {
        new QueryRates([[1,3]]);
    }

    public function testShouldQueryRatesMatch()
    {
        $qr = new QueryRates([[Currency::RUB, Currency::USD]]);

        $this->assertEquals($qr->getPairs(), [[Currency::RUB, Currency::USD]]);
    }

    public function testShouldCbrProviderRequest()
    {
        $provider = new CbrMockedProvider();
        $rates = $provider->query(new QueryRates([[Currency::RUB, Currency::USD]]));
        $this->assertEquals($rates[Currency::RUB][Currency::USD], 64.5463);
    }

    public function testShouldYahooProviderRequest()
    {
        $provider = new YahooMockedProvider();
        $rates = $provider->query(new QueryRates([[Currency::RUB, Currency::USD]]));
        $this->assertEquals($rates[Currency::RUB][Currency::USD], 64.8085);
    }

    public function testShouldGetRatesFromService()
    {
        $cbrProvider = new CbrMockedProvider();

        $service = new \App\Services\ExchangeRate\RateService($cbrProvider);
        $rates = $service->getRatesFor(new QueryRates([[Currency::RUB, Currency::USD]]));

        $this->assertEquals(count($rates), 1);
        $this->assertTrue(isset($rates[Currency::RUB]));
        $this->assertEquals(count($rates[Currency::RUB]), 1);
        $this->assertTrue($rates[Currency::RUB][0]->getName() == Currency::USD);
        $this->assertTrue($rates[Currency::RUB][0]->getValue() == 64.5463);
    }

    public function testShouldCreateAggService()
    {
        $cbrProvider = new CbrMockedProvider();
        $yahooProvider = new YahooMockedProvider();

        $groupProviders = new \App\Services\ExchangeRate\Providers\GroupProvider();
        $groupProviders->add([$yahooProvider, $cbrProvider]);

        $service = new \App\Services\ExchangeRate\RateService($groupProviders);

        $rates = $service->getRatesFor(new QueryRates([[Currency::RUB, Currency::USD]]));

        $this->assertEquals(count($rates), 1);
        $this->assertTrue(isset($rates[Currency::RUB]));
        $this->assertEquals(count($rates[Currency::RUB]), 1);
        $this->assertTrue($rates[Currency::RUB][0]->getName() == Currency::USD);
        $this->assertTrue($rates[Currency::RUB][0]->getValue() == 64.8085);
    }

    public function testShouldGetNextSourceForAggService()
    {
        $cbrProvider = new CbrMockedProvider();
        $yahooProvider = $this->createMock(YahooMockedProvider::class);

        $yahooProvider->method('query')->willThrowException(new \Exception());

        $groupProviders = new \App\Services\ExchangeRate\Providers\GroupProvider();
        $groupProviders->add([$yahooProvider, $cbrProvider]);

        $service = new \App\Services\ExchangeRate\RateService($groupProviders);

        $rates = $service->getRatesFor(new QueryRates([[Currency::RUB, Currency::USD]]));

        $this->assertEquals(count($rates), 1);
        $this->assertTrue(isset($rates[Currency::RUB]));
        $this->assertEquals(count($rates[Currency::RUB]), 1);
        $this->assertTrue($rates[Currency::RUB][0]->getName() == Currency::USD);
        $this->assertTrue($rates[Currency::RUB][0]->getValue() == 64.5463);
    }
}
