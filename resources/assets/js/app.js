"use strict";

require('./bootstrap');

require('./app/services');
require('./app/directives');
require('./app/controllers');

angular.module('tinyexchanger', ['ngRoute', 'tinyexchanger.controllers', 'tinyexchanger.directives'])

    .config(['$routeProvider', '$httpProvider', '$logProvider', '$resourceProvider', 'toastrConfig',
        function ($routeProvider, $httpProvider, $logProvider, $resourceProvider, toastrConfig) {

            $logProvider.debugEnabled(true);

            angular.extend(toastrConfig, {
                autoDismiss: true,
                containerId: 'toast-container',
                maxOpened: 0,
                timeOut: 3000,
                newestOnTop: true,
                positionClass: 'toast-top-right',
                preventDuplicates: false,
                preventOpenDuplicates: false,
                target: 'body'
            });

            angular.extend($httpProvider.defaults.headers.common, {
                "X-XSRF-TOKEN": window.Laravel.csrfToken
            });

            angular.extend($resourceProvider.defaults.actions, {
                update: {method: 'PUT'}
            });

            //interceptors
            var resolveRoute = {
                globalConfig: ['$http', 'config', function ($http, config) {
                    return $http.get('/api/config').then(function (response) {
                        return config.setConfig(response.data);
                    }, function (response) {
                        config.setConfig();
                    })
                }]
            };

            $routeProvider
                .when('/', {
                    resolve: resolveRoute,
                    controller: 'ExchangeRateCtrl',
                    templateUrl: 'views/exchangerate.html'
                })
                .otherwise('/');

        }])

    .run(['$rootScope', 'authService', '$window', function ($rootScope, authService, $window) {
        $rootScope.csrfToken = function () {
            return $window.Laravel.csrfToken;
        };
        $rootScope.isAuth = function () {
            return authService.isAuth();
        };
        $rootScope.user = function () {
            return authService.getUser();
        };
    }]);
