
window._ = require('lodash');

window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');

require('angular');
require('angular-resource');
require('angular-messages');
require('angular-cookies');
require('angular-animate');
require('angular-sanitize');
require('angular-route');

require('angular-toastr');
