"use strict";

angular.module('tinyexchanger.services', ['ng', 'ngAnimate', 'ngCookies', 'ngSanitize', 'ngMessages', 'ngResource', 'toastr'])

    .service('config', [function () {
        var config = null;

        this.getConfig = function () {
            return config;
        };

        this.setConfig = function (conf) {
            config = conf || null;
        };
    }])

    .service('authService', ['config', function (config) {
        this.isAuth = function () {
            return !!config.getConfig();
        };

        this.hasRoles = function (roles) {
            return config.getConfig() && config.getConfig().user && roles.indexOf(config.getConfig().user.role) > -1;
        };

        this.getUser = function () {
            return config.getConfig() && config.getConfig().user;
        }
    }])

    .service('exchangeRateSvc', ['$resource', '$log', function ($resource, $log) {
        this.get = $resource('/api/exchangerate/:id', { id: '@id' }).get;
    }])

;