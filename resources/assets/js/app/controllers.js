"use strict";

angular.module('tinyexchanger.controllers', ['tinyexchanger.services'])

    .controller('ExchangeRateCtrl', ['$scope', 'exchangeRateSvc', '$timeout', function ($scope, exchangeRateSvc, $timeout) {
        var timeoutRefresh = 9000,
            currentTimeoutUnFn = null;

        (function exchangeRateFn () {
            // Old school ajax long pulling :)
            exchangeRateSvc.get().$promise.then(function (r) {
                $scope.exchangeRate = r.data;
                currentTimeoutUnFn = $timeout(exchangeRateFn, timeoutRefresh);
            });
        })();

        $scope.$on('$destroy', function () {
            currentTimeoutUnFn && currentTimeoutUnFn();
        });
    }])

;