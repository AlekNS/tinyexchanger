@extends('layouts.app')

@section('content')
<nav-bar></nav-bar>
<div data-ng-view class="container">
  <div class="row">
    <h4 class="alert alert-info">Application loading...</h4>
  </div>
</div>
@endsection
